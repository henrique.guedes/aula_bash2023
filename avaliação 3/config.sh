#!/bin/bash

azul="\e[0;36m"
vermelho="\e[0;31m"
roxo="\e[0;35m"
verde="\e[0;32m"

read -p "Escolha a cor para o prompt:
1 Azul
2 Vermelho
3 Roxo
4 Verde
Digite sua escolha: " cor

if [ $cor -eq 1 ]; then
	read -p "Escolha o formato do prompt:
	1 Apenas o nome do usuario
	2 Nome do usuario e data
	3 Nome do usuario, data e hora
	4 Prompt em duas linhas com nome e hostname
	5 Digite uma frase qualquer para o prompt
	Digite sua escolha: " prom
	case $prom in
		1) novo_ps="$azul\u: \e[m";;
		2) novo_ps="$azul\u \d : \e[m";;
		3) novo_ps="$azul\u \d \t : \e[m";;
		4) novo_ps="$azul\u \n \h: \e[m";;
		5) read -p "digite a frase: " frase ; novo_ps="$azul$frase: \e[m";

	esac
elif [ $cor -eq 2 ]; then
	read -p "Escolha o formato do prompt:
	1 Apenas o nome do usuario
	2 Nome do usuario e data
	3 Nome do usuario, data e hora
	4 Prompt em duas linhas com nome e hostname
	5 Digite uma frase qualquer para o prompt
	Digite sua escolha: " prom
	case $prom in
		1) novo_ps="$vermelho\u: \e[m";;
		2) novo_ps="$vermelho\u \d : \e[m";;
		3) novo_ps="$vermelho\u \d \t : \e[m";;
		4) novo_ps="$vermelho\u \n \h: \e[m";;
		5) read -p "digite a frase: " frase ; novo_ps="$vermelho$frase: \e[m";

	esac
elif [ $cor -eq 3 ]; then
	read -p "Escolha o formato do prompt:
	1 Apenas o nome do usuario
	2 Nome do usuario e data
	3 Nome do usuario, data e hora
	4 Prompt em duas linhas com nome e hostname
	5 Digite uma frase qualquer para o prompt
	Digite sua escolha: " prom
	case $prom in
		1) novo_ps="$roxo\u: \e[m";;
		2) novo_ps="$roxo\u \d : \e[m";;
		3) novo_ps="$roxo\u \d \t : \e[m";;
		4) novo_ps="$roxo\u \n \h: \e[m";;
		5) read -p "digite a frase: " frase ; novo_ps="$roxo$frase: \e[m";

	esac

elif [ $cor -eq 4 ]; then
	read -p "Escolha o formato do prompt:
	1 Apenas o nome do usuario
	2 Nome do usuario e data
	3 Nome do usuario, data e hora
	4 Prompt em duas linhas com nome e hostname
	5 Digite uma frase qualquer para o prompt
	Digite sua escolha: " prom
	case $prom in
		1) novo_ps="$verde\u: \e[m";;
		2) novo_ps="$verde\u \d : \e[m";;
		3) novo_ps="$verde\u \d \t : \e[m";;
		4) novo_ps="$verde\u \n \h: \e[m";;
		5) read -p "digite a frase: " frase ; novo_ps="$verde$frase: \e[m";

	esac

	
fi


