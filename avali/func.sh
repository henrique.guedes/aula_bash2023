#!/bin/bash

linha() {
	linha_numero=$1
	arquivo1=$2
	cat $arquivo1 | sed -n "${linha_numero}p" "$arquivo"

}

coluna() {
	coluna_numero=$1
	arquivo2=$2
	cat $arquivo2 | cut -d ":" -f$coluna_numero

}
