#!/bin/bash
source novafuncao.sh
source funcao_dow.sh

yad --title "SCPDELUXE" --text="Seja bem vindo!" --button="OK:0" --width=300 --height=100

while [ "$?" -eq 0 ]
do
	acao=$(yad --title "O que deseja fazer?" \
		--list --column="Opção" \
		--column="Descrição" \
		"1" "Enviar um arquivo ou mais." \
		"2" "Baixar um arquivo ou mais." \
		--button="OK:0" --button="Cancelar:1" --width=300 --height=200)

	case "$(echo $acao | cut -d'|' -f1)" in
		"1") enviar;;
		"2") baixar;;
		*) yad --title "SCPDELUXE" --text "Nenhuma opção selecionada. Saindo..." --width=300 --height=100;break;
	esac

done

