#!/bin/bash
source novafuncao.sh


baixar() {
	local=$(yad --title="SCPDELUXE" --file-selection --directory --button="OK":0 --button="Cancelar":1)
 	entrada
	local_remoto=$(sshpass -p "$password" ssh -X $usuario@$ip 'yad --file --title="SCPDELUXE" --file-selection --multiple --file-filter="Todos os Arquivos | *" --button="OK":0 --button="Cancelar":1') && colu=$(echo "$local_remoto" | awk -F '|' '{print NF}')

	echo "$local_remoto" | awk -F'|' '{for (i=1; i<=NF; i++) print $i}' | while read -r caminho; do
	sshpass -p "$password" scp -r $usuario@$ip:"$caminho" $local 
	done
	
}

enviar() {
	arquivo=$(yad --file --title="SCPDELUXE" --file-selection --multiple --file-filter="Todos os Arquivos | *" --button="OK":0 --button="Cancelar":1)
 	entrada
	dir_remoto=$(sshpass -p "$password" ssh -X $usuario@$ip 'yad --file --title="SCPDELUXE" --file-selection --directory --button="OK":0 --button="Cancelar":1')

	echo "$arquivo" | awk -F'|' '{for (i=1; i<=NF; i++) print $i}' | while read -r texto; do
	arqu=$(basename "$texto")
	sshpass -p "$password" scp "$texto" $usuario@$ip:"$dir_remoto" 
	done
	
}



